package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.SedeRepository;
import domain.Sede;

public class JpaSedeRepository extends JpaBaseRepository<Sede, Long> implements
SedeRepository{

	@Override
	public Sede findByNumber(String number) {
		
		return null;
	}
	@Override
	public Collection<Sede> findByPersonId(Long personId) {
		return null;
	}

}
