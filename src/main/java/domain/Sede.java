package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Sede implements BaseEntity<Long> {
	
	@Id
	@SequenceGenerator(name = "sede_id_generator", sequenceName = "sede_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sede_id_generator")
	private Long id;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String direction;
	
	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String code;
	
	@OneToMany(mappedBy = "targetSede")
	private Collection<Sede> Sedes;
	
	@Override
	public Long getId() {
		return id;
	}
	@Override
	public void setId(Long id) {
		this.id = id;	
	}
	

}
